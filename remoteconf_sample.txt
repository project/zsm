settings:
  load_previous_state: False
  save_on_shutdown: False
  description: Test Configuration
  verbosity: 0

conf:
  plugins:
    RemoteConf:
      type: core
      module: api.remote_conf
      class: RemoteConf
      settings:
        conf_url: http://example.com/zsm/get-settings/[uuid]
        auth:
          location: "http://example.com/user/login?_format=json"
          name: username
          pass: password